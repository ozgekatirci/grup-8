/**
 * @file RangeSensor.cpp
 * @Author Ozge KATIRCI (ozgekatirci0@gmail.com)
 * @date January, 2021
 * @brief  Source file of abstract class that interface of laser and sonar ranges class.
 */
#include "RangeSensor.h"

 // function for set PioneerRobotAPI* robot
void RangeSensor::setAPI(PioneerRobotAPI *robot) {
	robotAPI =robot ;
}
//pointer type robotAPI object deleted.
RangeSensor::~RangeSensor() {
	delete robotAPI;
}