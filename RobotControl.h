/**
 * @file RobotControl.h
 * @Author Melisa DEMIRHAN (melisa.dmrhn2202@gmail.com)
 * @date January, 2021
 * @brief Header file of class that controlling the robot's moves.
 */
#ifndef ROBOTCONTROL_H
#define ROBOTCONTROL_H
#include <iostream>
#include"RangeSensor.h"
#include"RobotInterface.h"
#include "PioneerRobotAPI.h"
#include "SonarSensor.h"
#include "LaserSensor.h"
#include "Pose.h"
#include "Path.h"
#include"PioneerRobotInterface.h"
#include"RobotOperator.h"

using namespace std;
//! A class to controlling robot.
/*!
 This class controlling the robot's moves and stores Pose datas.
*/
class RobotControl {
private:

    Pose* position;/*!<position is created  as pose type pointer object*/
    Path* p;/*!<p is created  as path type pointer  object*/
    RobotOperator* admin;/*!< admin is created as  RobotOperator type pointer object.*/
public:
    bool access_control = false;/*!< access_control is created  as bool type variable and assign false to inital value.*/
    RangeSensor* sensorS;/*!<sensorS is created as  RangeSensor type pointer object.*/
    RangeSensor* sensorL;/*!< sensorL is created as  RangeSensor type pointer object.*/
    RobotInterface* robotinterface;/*!< robotinterface is created as  RobotInterface type pointer object.*/
    /** \brief objects are set and updated.
    */
    RobotControl(RobotInterface* robot, RangeSensor* sens, RangeSensor* sens1, Pose* pose);
    /** \brief pointer type objects deleted.
    */
    ~RobotControl();
    /** \brief function for connecting to robot.
     */
    bool connectRobot();
    /** \brief function for disconnecting from robot.
    */
    void disconnectRobot();
    /** \brief function for turning robot to left.
    */
    void turnLeft();
    /** \brief function for turning robot to right.
     */
    void turnRight();
    /** \brief function for moving robot forward.
      * \param speed_ is float argument to represent speed of robot.
     */
    void forward_(float speed);
    /** \brief function for print position's datas.
     */
    void print();
    /** \brief function for moving robot backward.
      * \param speed_ is float argument to represent speed of robot.
      */
    void backward(float speed);
    /** \brief function for get position.
     * \return position
     */
    Pose getPose();
    /** \brief function for setting position from given position.
       * \param Pose is pose argument to represent position.
      */
    virtual void setPose(Pose*);
    /** \brief function for stop robot's turning.
     */
    void stopTurn();
    /** \brief function for stop robot's move.
     */
    void stopMove();
    /** \brief function for update pose values.
    */
    void updatePose();
    /** \brief function for adding new pose to path.
    */
    bool addToPath();
    /** \brief function for delete all nodes which has path.
    */
    bool clearPath();
    /** \brief function for writing poses which path has to output file.
    */
    bool recordPath();
    /** \brief function for close access. If user entered right access code, access will be close and functions are not working.
    * \param accessCode is int argument to represent access code.
    */
    bool closeAccess(int accessCode);
    /** \brief function for open access. If user cannot enter right access code, RobotControl's functions are not working.
    * If user entered right access code, access will be open and functions are working rightly.
    * \param accessCode is int argument to represent access code.
    */
    bool openAccess(int accessCode);
};
#endif
