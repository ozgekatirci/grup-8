/**
 * @file RobotInterface.h
 * @Author Ozge Katirci (ozgekatirci0@gmail.com)
 * @date January, 2021
 * @brief Header file of  abstract class that interface for PineerRobotInterface.
 */

#ifndef ROBOTINTERFACE_H
#define ROBOTINTERFACE_H
#include"Node.h"
#include"Pose.h"
#include"RangeSensor.h"
#include"SonarSensor.h"
#include"LaserSensor.h"

 //! A  abstract class that interface for PineerRobotInterface.
 /*!
  This class controlling the robot's moves and stores Pose datas.
 */
class RobotInterface{
protected:
    Pose* position;/*!< position is created Pose type object. */
    RangeSensor* rangeSensorS; /*!< rangeSensorS is created RangeSensor* type object. */
    RangeSensor* rangeSensorL;/*!< rangeSensorL is created RangeSensor* type object. */
public:
    /** \brief pointer type object deleted.
    */
    ~RobotInterface();
    int state;/*!< state is created int type variable. */
    /** \brief virtual  function for connecting to robot.
    */
    virtual bool connectRobot() = 0;
    /** \brief virtual  function for disconnecting from robot.
    */
    virtual void disconnectRobot() = 0;
    /** \brief virtual  function for set sensors.
    * \param s is RangeSensor object .
      * \param a is RangeSensor object to.
   */
     virtual void setSensors(RangeSensor* s, RangeSensor* a)=0;
    /** \brief function for turning robot to left.
     */
    virtual void turnLeft() = 0;
    /** \brief function for turning robot to right.
     */
    virtual void turnRight() = 0;
    /** \brief function for moving robot forward.
      * \param speed_ is float argument to represent speed of robot.
     */
    virtual void forward_(float speed) = 0;
    /** \brief function for print position's datas.
     */
    virtual void print() = 0;
    /** \brief function for moving robot backward.
      * \param speed_ is float argument to represent speed of robot.
      */
    virtual void backward(float speed) = 0;
    /** \brief function for get position.
     * \return position
     */
    Pose getPose();
    /** \brief function for setting position from given position.
       * \param Pose is pose argument to represent position.
      */
    void setPose(Pose*);
    /** \brief function for stop robot's turning.
     */
    virtual void stopTurn() = 0;
    /** \brief function for stop robot's move.
     */
    virtual void stopMove() = 0;
     /** \brief function for updating sensors.
     */
    virtual void updateSensors() = 0;
     /** \brief function for updating pose.
     */
    virtual void updaterobot() = 0;
};

#endif

