/**
 * @file RobotInterface.cpp
 * @Author Ozge Katirci (ozgekatirci0@gmail.com)
 * @date January, 2021
 * @brief  Source file of  abstract class that interface for PineerRobotInterface.
 */
#include"RobotInterface.h"
#include"Pose.h"
#include"RangeSensor.h"
#include"LaserSensor.h"
#include"SonarSensor.h"

// function for set sensors.
void RobotInterface::setPose(Pose* pose) {
	   position = pose;
}
//function for get position.
Pose RobotInterface::getPose(){
	return *position;
}
//pointer type object deleted.
RobotInterface::~RobotInterface() {
	delete position, rangeSensorL, rangeSensorS;
}

