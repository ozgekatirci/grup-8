/**
 * @file Menu.cpp
 * @Author Ozge KATIRCI (ozgekatirci0@gmail.com), Melisa DEMIRHAN (melisa.dmrhn2202@gmail.com)
 * @date January, 2021
 * @brief Header file of class that control robot's all situations.
 */
#ifndef MENU_H
#define MENU_H
#include "RobotControl.h"
#include"Pose.h"
 //! A class to display menu.
 /*!
  This class display menu and calling function according to input that prompt from user.
 */
class Menu {
private:
    bool connection_control; /*!< connection_control is created bool type object to represent robot's connect situation. */
    static int move_distance;/*!< move_distance is created static int type variable to represent robot's distance covered. */
    RangeSensor* sens = new SonarSensor;/*!< sens is created as SonarSensor type object */
    RangeSensor* sens1 = new LaserSensor;/*!< sens1 is created as LaserSensor type object */
    RobotInterface* piooner = new PioneerRobotInterface;/*!< piooner is created as PioneerRobotInterface type object */
    Pose p;/*!< p is created as Pose type object */
    RobotControl* robot;
public:
    /** \brief robot is created as robotcontrol type object.
     */
    Menu();
    /** \brief pointer objects are deleting.
    */
    ~Menu();
    /** \brief function for display main menu.M
     */
    void display();
    /** \brief function for control robot's connect situation.
     */
    void connection();
    /** \brief function for control robot's moves.
     */
    void motion();
    /** \brief function for control sensors' datas.
     */
    void sensor();
    /** \brief function for exit from program.
     */
    void Quit();
    /** \brief function to close and open acces to robot with acces code taken by user.
     * \param accesscode is int argument to represent code for accesing to robot that is taken by user.
     */
    void access(int accesscode);

};

#endif;