/**
 * @file Node.h
 * @Author Esra DOGMAZ
 * @date January, 2021
 * @brief  Header file of class that Node.
 */
#ifndef NODE_H
#define NODE_H
#include <iostream>
#include "Pose.h"

using namespace std;

//! A class to create node.
/*!
	A class that take node's.
*/
class Node {
public:
	Node* next; /*!< next is Node* type object. */
	Pose pose; /*!< pose is Pose type object. */
};
#endif