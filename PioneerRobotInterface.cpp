/**
 * @file PioneerRobotInterface.cpp
 * @Author Melisa DEMIRHAN (melisa.dmrhn2202@gmail.com)
 * @date January, 2021
 * @brief Source file of class that directly connected to PioneerRobotAPI and based RobotInterface.
 */

#include <iostream>
#include"RobotInterface.h"
#include"PioneerRobotInterface.h"
#include"PioneerRobotAPI.h"
//function for connecting to robot.
bool PioneerRobotInterface::connectRobot(){
   return connect();
}
//function for disconnecting from robot.
void PioneerRobotInterface::disconnectRobot() {
    disconnect();
}
//function for turning robot to left.
void PioneerRobotInterface::turnLeft() {
    turnRobot(left); 
}
//function for turning robot to right.
void PioneerRobotInterface::turnRight() {
    turnRobot(right); 
}
//function for moving robot forward.
void PioneerRobotInterface::forward_(float speed) {
    state = 1;
    moveRobot(speed);
}   
//function for print position's datas.
void PioneerRobotInterface::print() {
    float temp;
    temp = getX();
	cout << "x: " << temp;
    temp = getY();
	cout << "y: " << temp;
    temp = getTh();
	cout << "Angle: " << temp;
}
//function for moving robot backward.
void PioneerRobotInterface::backward(float speed) {
    state = 1;
    speed *= -1;
    moveRobot(speed);
}
//function for get position.
Pose PioneerRobotInterface::getPose() {
    Pose p;
    p.setX(getX());
    p.setY(getY());
    p.setTh(getTh());
    return p;
}
//function for setting position from given position.
void PioneerRobotInterface::setPose(Pose* pose) {
    PioneerRobotAPI::setPose(pose->getX(), pose->getY(), pose->getTh());
}
//function for stop robot's turning.
void PioneerRobotInterface::stopTurn() {
    turnRobot(forward);
}
//function for stop robot's move.
void PioneerRobotInterface::stopMove() {
    state = 0;
    stopRobot();
}
//function for updating sensors.
void PioneerRobotInterface::updateSensors() {
    float a[16];
    getSonarRange(a);
    rangeSensorS->updateSensor(a);
    float b[1000];
    getLaserRange(b);
    rangeSensorL->updateSensor(b);
}
//function for updating pose.
void PioneerRobotInterface::updaterobot() {
    float myx, myy, myth;
	myx = getX();
	myy = getY();
	myth = getTh();
	position->setX(myx);
	position->setY(myy);
	position->setTh(myth);
}
//function for set sensors.
void PioneerRobotInterface::setSensors(RangeSensor* s, RangeSensor* a) {
    rangeSensorS = s;
    rangeSensorL = a;
}


