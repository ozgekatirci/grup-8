/**
 * @file PioneerRobotInterface.h
 * @Author Melisa DEMIRHAN (melisa.dmrhn2202@gmail.com)
 * @date January, 2021
 * @brief Header file of class that directly connected to PioneerRobotAPI.
 */

#ifndef PIONEERROBOTINTERFACE_H
#define PIONEERROBOTINTERFACE_H
#include <iostream>
#include"PioneerRobotAPI.h"
#include"RobotInterface.h"
#include"Node.h"
#include"Pose.h"

 //! A class to directly connect to PioneerRobotAPI and based RobotInterface.
 /*!
  This class controlling the robot's moves and stores Pose datas.
 */
class PioneerRobotInterface :public RobotInterface, public PioneerRobotAPI {
private:
 
public:
    /** \brief function for connecting to robot.
     * \return connection situation as false or true.
     */
    bool connectRobot();
    /** \brief function for disconnecting from robot.
    */
    void disconnectRobot();
    /** \brief function for turning robot to left.
    */
    void turnLeft();
    /** \brief function for turning robot to right.
     */
    void turnRight();
    /** \brief function for moving robot forward.
      * \param speed_ is float argument to represent speed of robot.
     */
    void forward_(float speed);
    /** \brief function for print position's datas.
     */
    void print();
    /** \brief function for moving robot backward.
      * \param speed_ is float argument to represent speed of robot.
      */
    void backward(float speed);
    /** \brief function for get position.
     * \return position
     */
    Pose getPose();
    /** \brief function for setting position from given position.
       * \param Pose is pose argument to represent position.
      */
    void setPose(Pose*);
    /** \brief function for stop robot's turning.
     */
    void stopTurn();
    /** \brief function for stop robot's move.
     */
    void stopMove();
    /** \brief function for updating sensors.
     */
    void updateSensors();
    /** \brief function for updating pose.
     */
    void updaterobot();
    /** \brief function for set sensors.
     * \param s is RangeSensor argument to represent SonarSensor.
      * \param a is RangeSensor argument to represent  LaserSensor.
     */
    void setSensors(RangeSensor* s, RangeSensor* a);
};

#endif
